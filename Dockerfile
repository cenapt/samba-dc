FROM debian:stretch-slim

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y samba krb5-user winbind
RUN rm /etc/samba/smb.conf
COPY run.sh /run.sh
RUN chmod +x /run.sh
VOLUME ['/var/lib/samba']
CMD /run.sh
