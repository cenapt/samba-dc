#!/bin/bash

if [ ! -f /etc/samba/smb.conf ]; then
    samba-tool domain provision --use-rfc2307 --realm=${REALM} \
               --domain=${DOMAIN} --server-role=dc --dns-backend=SAMBA_INTERNAL\
               --adminpass=${ADMIN_PASSWORD} &&
        cp /var/lib/samba/private/krb5.conf /etc/krb5.conf && awk '/^\[netlogon\]/ && !modif { printf("        tls enabled = yes\n        tls keyfile=/ssl/privkey.pem\n        tls certfile=/ssl/cert.pem\n        tls cafile = /ssl/chain.pem\n\n"); modif=1 } {print}' < /etc/samba/smb.conf > /tmp/smb.tmp && mv /tmp/smb.tmp /etc/samba/smb.conf 
fi

# We need our server to see itself
if [[ ! $(grep 127.0.0.1 /etc/resolv.conf) ]]; then 
    echo 'nameserver 127.0.0.1' | cat - /etc/resolv.conf > /tmp/resolv.conf && cat /tmp/resolv.conf >  /etc/resolv.conf && rm /tmp/resolv.conf
fi

chmod 600 /ssl/*
chown 0:0 /ssl/*

samba -i < /dev/null
