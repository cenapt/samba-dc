# cenapt/samba-dc

This docker image will start a Samba server (Active Directory and Domain Controller). You have to expose the ports you need.
It is currently only tested for LDAP using Let's encrypt certs.

## Example of use

docker run -it --rm -p 389:389 -p 636:636 -v /etc/letsencrypt/live/auth.example.com:/ssl -e DOMAIN=EXAMPLE -e REALM=AUTH.EXAMPLE.COM -e ADMIN_PASSWORD=TestIt -v /samba/db:/var/lib/samba -v /samba/conf:/etc/samba cenapt/samba-dc
